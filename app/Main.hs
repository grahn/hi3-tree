module Main where

import I3Socket
import UI

main :: IO ()
main = do
  hdl <- createI3Handle
  tree <- getI3Tree hdl
  runApp tree $ subscribeLoop hdl

