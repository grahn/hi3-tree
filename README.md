# hi3-tree
This program pretty prints the i3 layout tree to a terminal using [brick](https://hackage.haskell.org/package/brick) and [pretty-tree](https://hackage.haskell.org/package/pretty-tree).

hi3-tree subscribes to window events from i3 and updates the displayed tree when it receives events. The string for each node in the tree is the name (or type) of the i3 node, truncated to 20 characters, followed by a colon and then the layout type. For example:
```
                 1: splitv
                     |
              ----------------------
             /                      \
emacs@t400.my.domain: splith  urxvt: splith
```

## Keybindings
* `1` through `0`: display workspaces 1, 2, etc.
* `a`: display entire i3 layout tree
* arrow keys, `h`, `j`, `k`, `l`: scroll display
* `q`, `Esc`: quit program

## Building and Running
hi3-tree is built using [The Haskell Tool Stack](https://docs.haskellstack.org/en/stable/README/). To build, simply run from the repo's root directory:
```
stack setup
stack build
```
and to run:
```
stack exec hi3-tree-exe
```

## Contributing
I greatly appreciate any comments, criticism, or pull requests.
