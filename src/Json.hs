{-# LANGUAGE OverloadedStrings #-}

module Json
  ( drawI3Tree,
    toI3Node,
    filterI3Node,
    I3Node(I3Node)
  ) where

import           Data.Aeson
import           Data.Aeson.Types           (Parser)
import qualified Data.ByteString.Lazy       as BSL
import           Data.ByteString.Lazy.Char8 (pack)
import           Data.Maybe                 (fromJust, fromMaybe)
import           Data.Tree
import           Data.Tree.Pretty

data I3Node = I3Node {
  id      :: Int,
  type_   :: String,
  name    :: Maybe String,
  focused :: Bool,
  layout  :: String,
  nodes   :: [I3Node]
  } deriving Show

emptyI3Node :: I3Node
emptyI3Node = I3Node 0 "" Nothing False "" []

instance FromJSON I3Node where
    parseJSON = withObject "i3node" $ \v -> I3Node
        <$> v .: "id"
        <*> v .: "type"
        <*> v .:? "name"
        <*> v .: "focused"
        <*> v .: "layout"
        <*> v .: "nodes"

toI3Node :: BSL.ByteString -> I3Node
toI3Node s = case decoded of
  Nothing     -> emptyI3Node
  (Just node) -> node
  where
    decoded = decode s :: Maybe I3Node

toTree :: I3Node -> Tree String
toTree x = Node (getName x) (map toTree $ nodes x)
  where
    getName x = case name x of
                  Just result -> (take 20 result) ++ ": " ++ (layout x)
                  Nothing     -> (type_ x) ++ ": " ++ (layout x)

filterI3Node :: String -> I3Node -> I3Node
filterI3Node n node =
  case length results of
    0         -> emptyI3Node
    otherwise -> head results
  where
    results = filter (\x -> (fromMaybe "" $ (name x)) == n) $ workspaces node
    workspaces x = case type_ x of
                     "workspace" -> x : (concat (map workspaces $ nodes x))
                     otherwise   -> (concat (map workspaces $ nodes x))


drawI3Tree :: I3Node -> String
drawI3Tree x = drawVerticalTree $ toTree $ x
