module UI where

import           Control.Concurrent (forkIO)
import           Data.Char          (isDigit)
import qualified Graphics.Vty       as V

import           Brick.AttrMap      (attrMap)
import qualified Brick.BChan        as BChan
import qualified Brick.Main         as M
import           Brick.Types        (ViewportType (Both), Widget)
import qualified Brick.Types        as T
import           Brick.Widgets.Core (cached, str, viewport)

import           I3Socket
import           Json

data Name = Viewport1 deriving (Ord, Show, Eq)

data UIState = UIState {
  tree   :: I3Node,
  filter :: (I3Node -> I3Node)
  }

getFilter :: (I3Node -> I3Node) -> Char -> (I3Node -> I3Node)
getFilter f x = case isDigit x of
  True      -> (filterI3Node [x])
  otherwise -> f


drawUi :: UIState -> [Widget Name]
drawUi s = [w]
    where
        w = viewport Viewport1 Both $ cached Viewport1 $ str $ drawI3Tree $ (UI.filter s) $ (tree s)

vp1Scroll :: M.ViewportScroll Name
vp1Scroll = M.viewportScroll Viewport1


scrollAmount :: Int
scrollAmount = 5

scrollLeft :: T.EventM Name ()
scrollLeft = M.hScrollBy vp1Scroll (-scrollAmount)

scrollRight :: T.EventM Name ()
scrollRight = M.hScrollBy vp1Scroll (scrollAmount)

scrollUp :: T.EventM Name ()
scrollUp = M.vScrollBy vp1Scroll (-scrollAmount)

scrollDown :: T.EventM Name ()
scrollDown = M.vScrollBy vp1Scroll (scrollAmount)


appEvent :: UIState -> T.BrickEvent Name I3Event -> T.EventM Name (T.Next UIState)
appEvent s (T.VtyEvent (V.EvKey V.KDown []))  = scrollDown >> M.continue s
appEvent s (T.VtyEvent (V.EvKey V.KUp []))    = scrollUp >> M.continue s
appEvent s (T.VtyEvent (V.EvKey V.KRight [])) = scrollRight >> M.continue s
appEvent s (T.VtyEvent (V.EvKey V.KLeft []))  = scrollLeft >> M.continue s
appEvent s (T.VtyEvent (V.EvKey (V.KChar 'h') [])) = scrollLeft >> M.continue s
appEvent s (T.VtyEvent (V.EvKey (V.KChar 'j') [])) = scrollDown >> M.continue s
appEvent s (T.VtyEvent (V.EvKey (V.KChar 'k') [])) = scrollUp >> M.continue s
appEvent s (T.VtyEvent (V.EvKey (V.KChar 'l') [])) = scrollRight >> M.continue s
appEvent s (T.VtyEvent (V.EvKey (V.KChar 'q') [])) = M.halt s
appEvent s (T.VtyEvent (V.EvKey (V.KChar 'a') [])) = M.invalidateCache >> M.continue (UIState (tree s) id)
appEvent s (T.VtyEvent (V.EvKey (V.KChar x)   [])) = M.invalidateCache >> M.continue (UIState (tree s) (getFilter (UI.filter s) x))
appEvent s (T.VtyEvent (V.EvKey V.KEsc [])) = M.halt s
appEvent s (T.AppEvent (I3Event i)) = M.invalidateCache >> M.continue (UIState i (UI.filter s))
appEvent s _ = M.continue s


app :: M.App UIState I3Event Name
app =
    M.App { M.appDraw = drawUi
          , M.appStartEvent = return
          , M.appHandleEvent = appEvent
          , M.appAttrMap = const $ attrMap V.defAttr []
          , M.appChooseCursor = M.neverShowCursor
          }

runApp :: I3Node -> (BChan.BChan I3Event -> IO()) -> IO ()
runApp s f = do
  eventChan <- BChan.newBChan 10
  forkIO (f eventChan)
  finalState <- M.customMain
                (V.mkVty V.defaultConfig)
                (Just eventChan) app (UIState s id)
  return ()
