module I3Socket
  ( createI3Handle,
    getI3Tree,
    subscribeLoop,
    I3Event(I3Event)
  ) where

import           Control.Monad.Fix          (fix)
import           Data.Binary.Get            (getWord32host, runGet)
import           Data.Binary.Put            (putWord32host, runPut)
import           Data.Bits                  (shift, (.&.))
import qualified Data.ByteString            as BS
import qualified Data.ByteString.Lazy       as BSL
import           Data.ByteString.Lazy.Char8 (pack, unpack)
import           Json
import           Network.Socket
import           System.IO                  (Handle, IOMode (ReadWriteMode))
import           System.Process

import qualified Brick.BChan                as BChan

data I3Event = I3Event I3Node deriving (Show)

-- messages to i3 need to start with this string
magicString = "i3-ipc"
subscribeMsgType = 2
getTreeMsgType = 4
windowEventMsgType = 3

getI3SocketPath :: IO String
getI3SocketPath = do
  (_, Just hout, _, _) <- createProcess (proc "i3" ["--get-socketpath"])
                          { std_out = CreatePipe }
  result <- BSL.hGetContents hout
  let path = unpack result
  return $ filter (\x -> x /= '\n') path


verifyMsgType :: Int -> Int -> IO ()
verifyMsgType expected actual = do
  if actual == expected then return ()
    else fail "Received wrong message type in reply."


createGetTreeMsg :: BS.ByteString
createGetTreeMsg = BS.concat $ concat $ map BSL.toChunks [b0,b1,b2]
  where
    b0 = pack "i3-ipc"
    b1 = runPut $ putWord32host 0 -- payload length is 0
    b2 = runPut $ putWord32host $ fromIntegral getTreeMsgType

createSubscribeMsg :: BS.ByteString
createSubscribeMsg = BS.concat $ concat $ map BSL.toChunks [b0,b1,b2,b3]
  where
    payload = "[\"window\"]"
    b0 = pack "i3-ipc"
    b1 = runPut $ putWord32host $ fromIntegral $ length payload
    b2 = runPut $ putWord32host $ fromIntegral subscribeMsgType
    b3 = pack payload


createI3Handle :: IO System.IO.Handle
createI3Handle = do
  i3Address <- getI3SocketPath
  sock <- socket AF_UNIX Stream 0
  connect sock $ SockAddrUnix i3Address
  socketToHandle sock ReadWriteMode


sendRequest :: System.IO.Handle -> BS.ByteString -> IO ()
sendRequest hdl msg = do
  BS.hPut hdl msg


readFromI3 :: System.IO.Handle -> IO (Int, BSL.ByteString)
readFromI3 hdl = do
  -- TODO: more foolproof parsing
  -- magic string
  reply <- BSL.hGet hdl $ length magicString
  -- payload length
  reply <- BSL.hGet hdl 4
  let length = runGet getWord32host reply
  -- message type
  reply <- BSL.hGet hdl 4
  let msgType = fromIntegral $ runGet getWord32host reply
  -- payload
  reply <- BSL.hGet hdl $ fromIntegral length
  return (msgType, reply)


getI3Tree :: System.IO.Handle -> IO I3Node
getI3Tree hdl = do
  let msg = createGetTreeMsg
  sendRequest hdl msg
  (msgType, reply) <- readFromI3 hdl
  verifyMsgType getTreeMsgType msgType
  return $ toI3Node reply


subscribeLoop hdl eventChan = do
  let msg = createSubscribeMsg
  sendRequest hdl msg
  (msgType, reply) <- readFromI3 hdl
  -- TODO: verify reply
  fix $ \loop -> do
    (msgType, reply) <- readFromI3 hdl
    handleMsg msgType reply
    loop
      where
        handleMsg msgType reply =
          case (shift msgType (-31)) of
            1         -> processEvent (msgType .&. 255) reply
            0         -> handleReply msgType reply
            otherwise -> return ()
          where
            handleReply msgType reply
              | msgType == getTreeMsgType = BChan.writeBChan eventChan $ I3Event $ toI3Node reply
              | otherwise = return ()
            processEvent msgType reply
              | msgType == windowEventMsgType = sendRequest hdl createGetTreeMsg
              | otherwise = return ()
